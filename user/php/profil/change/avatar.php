<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/
 
session_start();

if(isset($_GET['id'])){
	$userId = $_GET['id'];
}


if(!isset($_SESSION["userId"])){
	echo "<p class='text-danger text-center'>Sie müssen sich erst <a href='../index.php'>hier</a> einloggen!</p>";
	exit();
} else if($_SESSION["userId"] !== $userId){
	echo "<p class='text-danger text-center'>Dieses Profil gehört nicht ihnen!</p>";
	exit();
}

$error = [];

if(file_exists("../../../../components/config/dbConnection.php")){
	require_once("../../../../components/config/dbConnection.php");
}

if(isset($_POST['changeAvatar'])){

	if(!empty($_FILES["newAvatar"]["name"])){

			//Bilderupload

			$upload_dir = "../../../images/";

			$pictureName = $_FILES['newAvatar']['name'];
			$pictureExtension = $_FILES['newAvatar']['name'];
			$pictureSize = $_FILES['newAvatar']['size'];
			$tmpPath = $_FILES['newAvatar']['tmp_name'];
			

			$pictureName = pathinfo($pictureName, PATHINFO_FILENAME);
			$extension = strtolower(pathinfo($pictureExtension, PATHINFO_EXTENSION));
			

			$allowedExtensions = array("png", "jpg", "jpeg", "gif");
			$allowedSize = 152000;
			
			

			if(!in_array($extension, $allowedExtensions)){
				$error[] = "<p class='text-danger text-center'>Sie können nur Bilder mit den Endungen png, jpg, jpeg, gif hochladen!</p>";
			}

			if($pictureSize > $allowedSize){
				$error[] = "<p class='text-danger text-center'>Die Bilder dürfen maximal nur 152 kb groß sein!</p>";
			}

			if(function_exists("exif_imagetype")){

				$mimeType = exif_imagetype($tmpPath);
				$allowedMimeTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);

				if(!in_array($mimeType, $allowedMimeTypes)){
					$error[] = "<p class='text-danger text-center'>Nur der Upload von Bilddateien ist gestattet</p>";
				}
			}
			
			$safePath = $upload_dir. $pictureName . '.' . $extension;
			$safePicforDB = "images/" . $pictureName . "." . $extension;


			if(file_exists($safePath)){
				$id = 1;

				do{
					$safePath = $upload_dir . $pictureName . '_' . $id .'.' . $extension;
					$safePicforDB = "images/" . $pictureName . '_' . $id ."." . $extension;
					$id++;
				} while (file_exists($safePath)); 	
			}

		}else{
			$fehler = "<p class='text-danger text-center'>Bitte laden sie ein Bild hoch!</p>";
		}



	if(!isset($fehler) && count($error) === 0 && isset($userId)){

		if(isset($tmpPath) && isset($safePath)){
				move_uploaded_file($tmpPath, $safePath);
			}
		
		try{

			$changeUsername = $dbv->prepare("UPDATE users SET avatar = :avatar WHERE userId = :userId");
			$result = $changeUsername->execute(
					array(
						":avatar" =>  $safePicforDB,
						":userId" => $userId
					)
			);

			if($result){
				header("Location: ../../profil.php?id=" . $userId . "&val=true");
			}

		}catch(EXCEPTION $a){
			echo "Ein Fehler ist aufgetreten: " . $a->getMessage();
			exit();
		}	
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Profilbild ändern</title>

	<link rel="stylesheet" href="../../../../libraries/bootstrap/css/bootstrap.min.css">

	<style>
		a.nav-link:hover{
			color: black !important;
		}
		
		button:hover{
			cursor: pointer;
		}
	</style>
</head>
<body>
	<header>
		<nav class="navbar bg-danger p-3">
           <a class="navbar-brand text-white nav-link" href='<?php echo "../profildata.php?id=" . $userId; ?>'>Zurück</a>
        </nav>
	</header>
	<main>
		<section class="container mt-5">
			<h1 class="text-center">Profilbild ändern</h1>
				<div class="mb-3">
					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Neues Profilbild:</label>
							<input type="file" name="newAvatar" class="form-control" id="pic">
						</div>

						<button name="changeAvatar" id="changeAvatar" class="btn btn-danger form-control">Profilbild ändern!</button>
					</form>
				</div>
				<?php
					if(isset($error) && count($error) > 0){
						echo implode("<br>", $error);
					} else if(isset($fehler)){
						echo $fehler;
					}
				?>
		</section>
	</main>
	<footer class="mt-5">
		<p class="text-center"> &copy; Stef 2018</p>
	</footer>
</body>
</html>