<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/
 
session_start();

if(isset($_GET['id'])){
	$userId = $_GET['id'];
}

if(!isset($_SESSION["userId"])){
	echo "<p class='text-danger text-center'>Sie müssen sich erst <a href='../index.php'>hier</a> einloggen!</p>";
	exit();
} else if($_SESSION["userId"] !== $userId){
	echo "<p class='text-danger text-center'>Dieses Profil gehört nicht ihnen!</p>";
	exit();
}

if(file_exists("../../../../components/config/dbConnection.php")){
	require_once("../../../../components/config/dbConnection.php");
}

if(isset($_POST['changeUsername'])){

	$newUsername = isset($_POST['newUsername']) ? $_POST['newUsername'] : null;


	if(empty($newUsername)){
		$fehler = "<p class='text-danger text-center'>Bitte füllen sie das Feld aus!</p>";
	} else{
		try{

				$selectAllUsernames = $dbv->prepare("SELECT username FROM users");
				$selectAllUsernames->execute();

				while($row = $selectAllUsernames->fetch(PDO::FETCH_ASSOC)){
					if(isset($newUsername)){
						if($row["username"] === $newUsername){
								$fehler = "<p class='text-danger text-center'>Dieser Username ist bereits vorhanden! Bitte nehmen sie einen anderen!</p>";
						}
					}
				}
				
		}catch(EXCEPTION $w){
			echo "Ein Fehler ist aufgetreten: " . $w->getMessage();
			exit();
		}	
	}

	if(!isset($fehler) && isset($userId)){

		try{


			$changeUsername = $dbv->prepare("UPDATE users SET username = :username WHERE userId = :userId");
			$result = $changeUsername->execute(
					array(
						":username" => $newUsername,
						":userId" => $userId
					)
			);

			if($result){
				header("Location: ../../profil.php?id=" . $userId);
			}

		}catch(EXCEPTION $a){
			echo "Ein Fehler ist aufgetreten: " . $a->getMessage();
			exit();
		}

	}
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Username ändern</title>

	<link rel="stylesheet" href="../../../../libraries/bootstrap/css/bootstrap.min.css">

	<style>
		a.nav-link:hover{
			color: black !important;
		}

		button:hover{
			cursor: pointer;
		}
	</style>
</head>
<body>
	<header>
		<nav class="navbar bg-warning p-3">
           <a class="navbar-brand text-white nav-link" href='<?php echo "../profildata.php?id=" . $userId; ?>'>Zurück</a>
        </nav>
	</header>
	<main>
		<section class="container mt-5">
			<h1 class="text-center">Username ändern</h1>
				<div class="mb-3">
					<form method="post">
						<div class="form-group">
							<label>Neuer Username:</label>
							<input type="text" name="newUsername" class="form-control">
						</div>
					

						<button name="changeUsername" class="form-control btn btn-warning">Username ändern!</button>
					</form>
				</div>
				<?php
					if(isset($fehler)){
						echo $fehler;
					}
				?>
		</section>
	</main>
	<footer class="mt-5">
		<p class="text-center"> &copy; Stef 2018</p>
	</footer>
</body>
</html>