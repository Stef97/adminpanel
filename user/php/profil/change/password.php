<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/

session_start();

if(!isset($_SESSION["userId"])){
	echo "<p  class='text-danger text-center'>Sie müssen sich erst <a href='../index.php'>hier</a> einloggen!</p>";
	exit();
} 

if(isset($_GET['id'])){
	$userId = $_GET['id'];
}

if($_SESSION["userId"] !== $userId){
	echo "<p  class='text-danger text-center'>Dieses Profil gehört nicht ihnen!</p>";
	exit();
}

if(file_exists("../../../../components/config/dbConnection.php")){
	require_once("../../../../components/config/dbConnection.php");
}

if(isset($_POST['changePass'])){

	$newPassword = isset($_POST['newPassword']) ? $_POST['newPassword'] : null;
	$repeatedPassword = isset($_POST['repeatPassword']) ? $_POST['repeatPassword'] : null;

	if(empty($newPassword) || empty($repeatedPassword)){
		$fehler = "<p  class='text-danger text-center'>Bitte füllen sie alle Felder aus!</p>";
	} else if($newPassword !== $repeatedPassword){
		$fehler = "<p  class='text-danger text-center'>Die Passwörter stimmen nicht überein!</p>";
	}

	if(!isset($fehler) && isset($userId)){

		$pepper = "!#+?=45&/()";
		$password = $newPassword . $pepper;

		$password = password_hash($password, PASSWORD_DEFAULT);

		try{

			$changePassword = $dbv->prepare("UPDATE users SET passwort = :password WHERE userId = :userId");
			$result = $changePassword->execute(
								array(
									":password" => $password,
									":userId" => $userId
								)
						);
			if($result){
				header("Location: ../../profil.php?id=" . $userId);
			}

		}catch(EXCEPTION $w){
			echo "Ein Fehler ist aufgetreten: " . $w->getMessage();
			exit();
		}
	}

}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Passwort ändern</title>

	<link rel="stylesheet" href="../../../../libraries/bootstrap/css/bootstrap.min.css">

	<style>
		a.nav-link:hover{
			color: black !important;
		}
		
		button:hover{
			cursor: pointer;
		}
	</style>
</head>
<body>
	<header>
		<nav class="navbar bg-primary p-3">
           <a class="navbar-brand text-white nav-link" href='<?php echo "../profildata.php?id=" . $userId; ?>'>Zurück</a>
        </nav>
	</header>
	<main>
		<section class="container mt-5">
			<h1 class="text-center">Passwort ändern</h1>
				<div class="mb-5">
					<form method="post">
						<div class="form-group">
							<label>Neues Passwort:</label>
							<input type="password" name="newPassword" class="form-control">
						</div>
						
						<div class="form-group">
							<label>Passwort wiederholen:</label>
							<input type="password" name="repeatPassword" class="form-control">
						</div>

						<button name="changePass" class="form-control btn btn-primary">Passwort ändern!</button>
					</form>
				</div>
				<?php
					if(isset($fehler)){
						echo $fehler;
					}
				?>
		</section>	
	</main>
	<footer class="mt-5">
		<p class="text-center"> &copy; Stef 2018</p>
	</footer>
</body>
</html>