<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/

session_start();

if(isset($_GET['id'])){
	$userId = $_GET['id'];
}

if(!isset($_SESSION["userId"])){
	echo "<p class='text-danger text-center'>Sie müssen sich erst <a href='../index.php'>hier</a> einloggen!</p>";
	exit();
} else if($_SESSION["userId"] !== $userId){
	echo "<p class='text-danger text-center'>Dieses Profil gehört nicht ihnen!</p>";
	exit();
}

if(isset($userId)){

	if(isset($_POST['changePassword'])){

		header("LOCATION: change/password.php?id=". $userId);

	}else if(isset($_POST['changeUsername'])){

		header("LOCATION: change/username.php?id=". $userId);	

	}else if(isset($_POST['changeAvatar'])){

		header("LOCATION: change/avatar.php?id=". $userId);	

	}
}	


?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ändere Profildaten</title>

	<link rel="stylesheet" href="../../../libraries/bootstrap/css/bootstrap.min.css">

	<style>
		a.nav-link:hover{
			color: black !important;
		}

		button:hover{
			cursor: pointer;
		}
	</style>
</head>
<body>
	<header>
		<nav class="navbar bg-primary p-3">
           <a class="navbar-brand text-white nav-link" href="../profil.php?id=<?php echo $userId; ?>">Zurück</a>
        </nav>
	</header>
	<main>
		<section class="container mt-5">
			<h1 class="text-center">Profildatenzentrale</h1>
			<form method="post" class="mt-5">
				<div class="row mb-4">
					<button name="changePassword" class="form-control btn btn-primary">Passwort ändern</button>
				</div>
				<div class="row mb-4">
					<button name="changeUsername" class="form-control btn btn-warning">Usernamen ändern</button>
				</div>
				<div class="row">
					<button name="changeAvatar" class="form-control btn btn-danger">Profilbild ändern!</button>
				</div>
			</form>
		</section>
	</main>
	<footer class="mt-5">
		<p class="text-center"> &copy; Stef 2018</p>
	</footer>
</body>
</html>