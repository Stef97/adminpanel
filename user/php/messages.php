<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/

session_start();

if(!isset($_SESSION["userId"])){
	echo "<p>Sie müssen sich erst <a href='../index.php'>hier</a> einloggen!</p>";
	exit();
}

if(file_exists("../../components/config/dbConnection.php")){
	require_once("../../components/config/dbConnection.php");
}

try{

	$selectMessages = $dbv->prepare("SELECT u.username,
											m.messageHead, 
											m.message, 
											m.datum 
									 FROM 
									 		users u
									 LEFT JOIN 
									 		messages m ON m.senderId = u.userId
									 WHERE 
									 	 	m.isDeleted = 0");
	
	$selectMessages->execute();
	$messages = $selectMessages->fetchAll(PDO::FETCH_ASSOC);

} catch(EXCEPTION $e){
	echo "Ein Fehler ist aufgetreten: " . $e->getMessage();
	exit();
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Alle Nachrichten</title>

	<link rel="stylesheet" href="../../libraries/bootstrap/css/bootstrap.min.css">

	<style>
		a.nav-link:hover{
			color: black !important;
		}
	</style>
</head>
<body>
	<header>
		<nav class="navbar bg-primary p-3">
           <a class="navbar-brand text-white nav-link" href="../home.php">Zurück</a>
        </nav>
	</header>
	<main>
		<section class="mt-5">
			<div class="container">
				<h2 class="text-center">Alle Nachrichten</h2>
					<div class="mt-5">
			<?php 
				if(isset($messages)){
					foreach ($messages as $key => $value) {

						$key += 1;

						$datetime = explode(" ", $value["datum"]);

						$tag = new DateTime($datetime[0]);
						$tag = $tag->format("d.m.Y");

						$uhrzeit = $datetime[1];

					
						echo "<article id='allMessages' class='mb-2'>";
							echo "<ul class='list-group'>";
								echo "<li class='list-group-item border-blue bg-primary text-dark font-weight-bold'>Nachricht ". $key . "</li>";
								echo "<li class='list-group-item border-blue'>Nachrichtengrund: " . htmlspecialchars($value["messageHead"]) . "</li>";
								echo "<li class='list-group-item border-blue'>Nachricht: " . htmlspecialchars($value['message']) . "</li>";
								echo "<li class='list-group-item border-blue'>Erstellt am " . htmlspecialchars($tag) . " um " . htmlspecialchars($uhrzeit) . " Uhr durch ". htmlspecialchars($value["username"]) . "</li>";
							echo "</ul>";
						echo "</article>";
					}
				}
			?>
					</div>
			</div>
		</section>
	</main>
	<footer class="mt-5">
		<p class="text-center"> &copy; Stef 2018</p>
	</footer>
</body>
</html>