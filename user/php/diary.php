<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/

session_start();

if(!isset($_SESSION["userId"])){
	echo "<p class='text-danger text-center'>Sie müssen sich erst <a href='../index.php'>hier</a> einloggen!</p>";
	exit();
}

if(file_exists("../../components/config/dbConnection.php")){
	require_once("../../components/config/dbConnection.php");
}

try{

	$selectTermine = $dbv->prepare("SELECT grund, beschreibung, datum FROM termine WHERE isDeleted = 0");
	$selectTermine->execute();

	$termine = $selectTermine->fetchAll(PDO::FETCH_ASSOC);
			

} catch(EXCEPTION $e){
	echo "Ein Fehler ist aufgetreten: " . $e->getMessage();
	exit();
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Alle Termine</title>

	<link rel="stylesheet" href="../../libraries/bootstrap/css/bootstrap.min.css">

	<style>
		a.nav-link:hover{
			color: black !important;
		}
	</style>
</head>
<body>
	<header>
		<nav class="navbar bg-primary p-3">
           <a class="navbar-brand text-white nav-link" href="../home.php">Zurück</a>
        </nav>
	</header>
	<main>
		<section class="mt-5">
			<div class="container">
				<h2 class="text-center">Alle Termine</h2>
					<div class="mt-5">
						<?php
							if(isset($termine)){
								foreach ($termine as $key => $value) {

									$key += 1;
									
									$date = explode(" ", $value["datum"]);

									$day = new DateTime($date[0]);
									$day = $day->format("d.m.Y");

									$time = substr($date[1], 0, 5);

									echo "<article id='alleTermine' class='mb-2'>";
											echo "<ul class='list-group'>";
												echo "<li class='list-group-item border-blue bg-primary text-dark font-weight-bold'>Termin ". $key . "</li>";
												echo "<li class='list-group-item border-blue'>Termingrund: " . htmlspecialchars($value["grund"]) . "</li>";
												echo "<li class='list-group-item border-blue'>Beschreibung: " . htmlspecialchars($value["beschreibung"]) . "</li>";
												echo "<li class='list-group-item border-blue'>Am: " . htmlspecialchars($day) . "</li>";
												echo "<li class='list-group-item border-blue'>Uhrzeit: " . htmlspecialchars($time) . "</li>";
											echo "</ul>";
										echo "</article>";
								}
							}
						?>
					</div>
			</div>
		</section>
	</main>
	<footer class="mt-5">
		<p class="text-center"> &copy; Stef 2018</p>
	</footer>
</body>
</html>