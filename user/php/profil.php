<?php 
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/

session_start();

if(!isset($_SESSION["userId"])){
	echo "<p class='text-danger text-center'>Sie müssen sich erst <a href='../index.php'>hier</a> einloggen!</p>";
	exit();
}

if(isset($_GET['id'])){
	$userId = $_GET['id'];

	if(file_exists("../../components/config/dbConnection.php")){
		require_once("../../components/config/dbConnection.php");
	}

	try{

		$selectUserData = $dbv->prepare("SELECT username, rang, avatar FROM users WHERE userId = :userId LIMIT 1");
		$selectUserData->execute(array(":userId" => $userId));

		$userData = $selectUserData->fetch(PDO::FETCH_ASSOC);

	} catch(EXCEPTION $e){
		echo "Ein Fehler ist aufgetreten: " . $e->getMessage();
		exit();
	}

	if($_SESSION['userId'] === $userId){
			$editProfil = "<li class='nav-item'><a href='profil/profildata.php?id=". $userId . "' class='nav-link text-white'>Profil bearbeiten</a></li>";
	} 
}




?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Profil von <?php echo ucfirst(htmlspecialchars($userData['username'])); ?></title>

	<link rel="stylesheet" href="../../libraries/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">


	<style>
		.white{
			background-color: white;
		}

		button:hover{
			cursor: pointer;
		}

		a.nav-link:hover{
			color: black !important;
		}

		img.size{
			width: 30em;
			height: 20em;
		}
	</style>
</head>
<body>
	<header>
		<nav class="navbar navbar-toggleable-md bg-primary p-3 ">
			<button class="navbar-toggler ml-auto white" type="button" data-toggle="collapse" data-target="#myNavbar" aria-controls="myNavbar" aria-expanded="false" aria-label="Toggle navigation">
    			<i class="fas fa-bars fa-2x bg-white"></i>
 			 </button>

 			 <div class="collapse navbar-collapse" id="myNavbar">
	           <ul class="navbar-nav">
					<li class="nav-item"><a href="../home.php"  class="nav-link text-white">Home</a></li>
					<?php
						if(isset($editProfil)){
							 echo $editProfil;
						}
					?>
				</ul>
			</div>
        </nav>
	</header>
	<main>
		<div class="container text-center mt-5 border border-primary">
			<h1 class="mb-5">Dein Profil</h1>
			<?php 
				echo "<figure class='figure'>";
					echo"<img class='figure-img img-fluid rounded size mb-5' src='../" . htmlspecialchars($userData['avatar']) . "' alt='profilPic'>";
					echo "<figcaption class='figure-caption'>";
						 	echo "<p> Username: " . ucfirst(htmlspecialchars($userData['username'])) . "</p>";
						 	echo "<p> Rang: " . ucfirst(htmlspecialchars($userData['rang'])) . "</p>";
					echo "</figcaption>";
				echo "</figure>"; 
			?>	
		</div>
	</main>
	<footer class="mt-5">
		<p class="text-center"> &copy; Stef 2018</p>
	</footer>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>
</html>