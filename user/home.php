<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/

session_start();

if(!isset($_SESSION["userId"])){
	echo "<p class='text-danger text-center'>Sie müssen sich erst <a href='../index.php'>hier</a> einloggen!</p>";
	exit();
}


$userId = $_SESSION["userId"];

if(file_exists("../components/config/dbConnection.php")){
	require_once("../components/config/dbConnection.php");
}

try{

	$selectUserData = $dbv->prepare("SELECT username, passwort, rang, avatar FROM users WHERE userId = :userId LIMIT 1");
	$selectUserData->execute(array(":userId" => $userId));

	$userData = $selectUserData->fetch(PDO::FETCH_ASSOC);

	// 1. Alle Nachrichten der letzten 2 Wochen anzeigen
	$selectMessages = $dbv->prepare("SELECT u.username,
											m.messageHead, 
											m.message, 
											m.datum 
									 FROM 
									 		users u
									 LEFT JOIN 
									 		messages m ON m.senderId = u.userId
									 WHERE 
									 		m.datum >= DATE_SUB(NOW(), INTERVAL 12 DAY) AND m.isDeleted = 0");
	
	$selectMessages->execute();
	$messages = $selectMessages->fetchAll(PDO::FETCH_ASSOC);


	// 2. Alle Termine welche in der Zukunft liegen anzeigen 
	$selectTermine = $dbv->prepare("SELECT grund, beschreibung, datum FROM termine WHERE datum >= NOW() AND isDeleted = 0");
	$selectTermine->execute();

	$termine = $selectTermine->fetchAll(PDO::FETCH_ASSOC);
			

} catch(EXCEPTION $e){
	echo "Ein Fehler ist aufgetreten: " . $e->getMessage();
	exit();
}

if(isset($_POST['logout'])){
	session_destroy();

	header("LOCATION: ../");
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Home</title>

	<link rel="stylesheet" href="../libraries/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

	<style>

		.border-blue{
			border: 1px solid blue;
		}

		button.hover{
			transition: 0.2s ease-in;
		}

		button.hover:hover{
			color: blue !important;
			background-color: white;
			cursor: pointer;
		}

		.white{
			background-color: white;
		}

		button:hover{
			cursor: pointer;
		}

		a.nav-link:hover{
			color: black !important;
		}

		.userPic{
			height: 2em;
			width: 2.5em;
		}
	</style>
</head>
<body>
	<header>
		<nav class="navbar navbar-toggleable-md navbar justify-content-between bg-info p-3">
      			<span class="navbar-brand font-weight-bold"><?php echo "Hallo " . htmlspecialchars(ucfirst($userData["username"])); ?> <img src="<?php echo htmlspecialchars($userData['avatar']);?>" class='userPic' alt="userPic"></span>
      			
      				<button class="navbar-toggler ml-auto white" type="button" data-toggle="collapse" data-target="#myNavbar" aria-controls="myNavbar" aria-expanded="false" aria-label="Toggle navigation">
    					<i class="fas fa-bars fa-2x bg-white"></i>
 			 		</button>
      			
      			
    		
			<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item active mr-2"><a href="<?php echo 'php/profil.php?id=' . $userId; ?>" class="nav-link text-white">Mein Profil</a></li>
						<li class="nav-item mr-2"><a href="php/messages.php" class="nav-link text-white">Alle Nachrichten</a></li>
						<li class="nav-item mr-2"><a href="php/diary.php" class="nav-link text-white">Alle Termine</a></li>
						<?php
							if($userData["rang"] === "admin"){
								echo "<li class='nav-item mr-2'><a href='../admin/adminpanel.php' class='nav-link text-white'>Zum Adminpanel</a></li>";
							}
						?>
						
						<form method="post" class="form-inline">
							<li><button name="logout" class="btn btn-outline-primary text-white hover">Logout</button></li>
						</form>
					</ul>
			</div>
		</nav>
	</header>
	<main>
		<section class="container-fluid mt-5">
			<div class="row">
				<div class="col-sm-6 col-12">
					<p class="h2 text-center">Ihre Nachrichten</p>
					<p class="text-center">Info: Es werden hier nur alle Nachrichten der letzten 2 Wochen angezeigt!</p>
					<?php 
						if(isset($messages)){
							foreach ($messages as $key => $value) {

								$key += 1;

								$datetime = explode(" ", $value["datum"]);

								$tag = new DateTime($datetime[0]);
								$tag = $tag->format("d.m.Y");

								$uhrzeit = $datetime[1];

							
								echo "<article id='allMessages' class='mb-2'>";
									echo "<ul class='list-group'>";
										echo "<li class='list-group-item border-blue bg-primary text-dark font-weight-bold'>Nachricht ". $key . "</li>";
										echo "<li class='list-group-item border-blue'>Nachrichtengrund: " . htmlspecialchars($value["messageHead"]) . "</li>";
										echo "<li class='list-group-item border-blue'>Nachricht: " . htmlspecialchars($value['message']) . "</li>";
										echo "<li class='list-group-item border-blue'>Erstellt am " . htmlspecialchars($tag) . " um " . htmlspecialchars($uhrzeit) . " Uhr durch ". htmlspecialchars($value["username"]) . "</li>";
									echo "</ul>";
								echo "</article>";
							}
						}
					?>
					
				</div>
				<div class="col-sm-6 col-12">
					<p class="h2 text-center">Ihre bevorstehenden Termine</p>
					<p class="text-center">Info: Es werden hier nur alle bevorstehende Termine angezeigt!</p>

					<?php
						if(isset($termine)){
							foreach ($termine as $key => $value) {
								
								$key += 1;

								$date = explode(" ", $value["datum"]);

								$day = new DateTime($date[0]);
								$day = $day->format("d.m.Y");

								$time = substr($date[1], 0, 5);

								echo "<article id='alleTermine' class='mb-2'>";
									echo "<ul class='list-group'>";
										echo "<li class='list-group-item border-blue bg-primary text-dark font-weight-bold'>Termin ". $key . "</li>";
										echo "<li class='list-group-item border-blue'>Termingrund: " . htmlspecialchars($value["grund"]) . "</li>";
										echo "<li class='list-group-item border-blue'>Beschreibung: " . htmlspecialchars($value["beschreibung"]) . "</li>";
										echo "<li class='list-group-item border-blue'>Am: " . htmlspecialchars($day) . "</li>";
										echo "<li class='list-group-item border-blue'>Uhrzeit: " . htmlspecialchars($time) . "</li>";
									echo "</ul>";
								echo "</article>";
							}
						}
					?>
				</div>
			</div>
			
		</section>
	</main>
	<footer class="text-center mt-5">
		<p> &copy; Stef 2018</p>
	</footer>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>
</html>