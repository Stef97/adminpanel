<?php

function checkValidTime($time){
	
	$partsOfTime = explode(":", $time);

	if(count($partsOfTime) < 2 || !is_numeric($partsOfTime[0]) || !is_numeric($partsOfTime[1]) || $partsOfTime[0] < 0 || $partsOfTime[0] > 24 || $partsOfTime[1] < 0 || $partsOfTime[1] > 59 || !preg_match("/^[0-9:]{5}$/", $time)){
		return false;
	} else{
		return true;
	}
}

?>