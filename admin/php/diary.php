<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/

session_start();


if(!isset($_SESSION['userId'])){

	echo "<p class='text-danger text-center'>Sie müssen sich erst <a href='../../index.php'>hier</a> einloggen!</p>";
	exit();

} else if(isset($_SESSION["rang"])){

	if($_SESSION['rang'] !== "admin"){
		echo "<p class='text-danger text-center'>Dieser Bereich ist für Sie nicht zugängig!</p>";
		exit();
	}
}

if(file_exists("../../components/config/dbConnection.php")){
	require_once("../../components/config/dbConnection.php");
}

if(file_exists("../../components/functions.php")){
	require_once("../../components/functions.php");
}

try{

	$deletedTime = date("Y-m-d");
	
	if(isset($_POST['deleteTermin'])){

			
			$elements = isset($_POST['id']) ? $_POST['id'] : null;
			

			if(empty($elements)){
				$fehler2 = "<p class='text-danger text-center'>Bitte wählen sie die Termine, welche Sie löschen möchten, über die Checkboxen aus!</p>";
			}

			if(!isset($fehler2)){

				foreach ($elements as $key => $terminId) {
					$deleteTermin = $dbv->prepare("UPDATE termine SET isDeleted = :isDeleted, deletedAt = :deletedAt WHERE terminId = :terminId");
					$deleteTermin->execute(
						array(
							":isDeleted" => 1,
							":deletedAt" => $deletedTime,
							":terminId" => $terminId
						)
					);
				}
			}
			
		
	} 
	else if(isset($_POST['editTermin'])){

		$terminId = isset($_POST['id']) ? $_POST['id'] : null;

		if(empty($terminId)){
			$fehler2 = "<p class='text-danger text-center'>Bitte wählen sie den Termin, welchen Sie editieren möchten, über die Checkboxen aus!</p>";
		} else if(count($terminId) > 1){
			$fehler2 = "<p class='text-danger text-center'>Sie können nicht mehrere Termine gleichzeitig editieren!</p>";
		}

		if(!isset($fehler2)){

			foreach ($terminId as $key => $terminId) {
				header("LOCATION: termine/edit_termin.php?id=" . $terminId);	
			}
			
		}

		
	}else if(isset($_POST['backup'])){

		$backupStmt = $dbv->prepare("UPDATE termine SET isDeleted = 0 WHERE deletedAt = :deletedAt");
		$result2 = $backupStmt->execute(array(":deletedAt" => $deletedTime));

		if($result2){
				$success2 = "<p class='text-success text-center'>Gelöschte Nachrichten wurden wieder hergestellt!</p>";
		}
	}

	

} catch(EXCEPTION $e){
	echo "Ein Fehler ist aufgetreten: " . $e->getMessage();
	exit();
}


if(isset($_POST['submitted'])){

	$termingrund = isset($_POST['termingrund']) ? $_POST['termingrund'] : null;
	$terminbeschreibung  = isset($_POST['terminbeschreibung']) ? $_POST['terminbeschreibung'] : null;
	$datum = isset($_POST['datum']) ? $_POST['datum'] : null;
	$uhrzeit = isset($_POST['uhrzeit']) ? $_POST['uhrzeit'] : null;
	
	$dateNow = date("Y-m-d H:i:s");

	if(isset($datum) && isset($uhrzeit)){
		$date = $datum . " " . $uhrzeit . ":00";
	}

	if(empty($termingrund) || empty($terminbeschreibung) || empty($datum) || empty($uhrzeit)){

		$fehler = "<p class='text-danger text-center'>Bitte füllen Sie alle Felder aus!</p>";

	} else if(strtotime($date) < strtotime($dateNow)){

		$fehler = "<p class='text-danger text-center'>Das Datum und die Uhrzeit müssen in der Zukunft liegen!</p>";

	} else if(!checkValidTime($uhrzeit)){

		$fehler = "<p class='text-danger text-center'>Bitte geben Sie eine gültige Uhrzeit an!</p>";
	}

	if(!isset($fehler)){


		
		$datum = new DateTime($date);
		$datum = $datum->format("Y-m-d H:i:s");
				

		try{

			$insertTermin = $dbv->prepare("INSERT INTO termine(grund, beschreibung, datum, isDeleted, deletedAt) VALUES (:grund, :beschreibung, :datum, :isDeleted, :deletedAt)");
			$insertTermin->execute(
					array(
						":grund" => $termingrund, 
						":beschreibung" => $terminbeschreibung, 
						":datum" => $datum,
						":isDeleted" => 0,
						":deletedAt" => null		
					)
			);

		}catch(EXCEPTION $w){
			echo "Ein Fehler ist aufgetreten: " . $w->getMessage();
			exit();
		}
	}
}

try{

	$selectTermin = $dbv->prepare("SELECT terminId, grund, beschreibung, datum FROM termine WHERE isDeleted = 0 ORDER BY datum ASC");
	$selectTermin->execute(array(":deletedAt" => $deletedTime));

	$alleTermine = $selectTermin->fetchAll(PDO::FETCH_ASSOC);

	$selectDeletedData = $dbv->prepare("SELECT terminId FROM termine WHERE isDeleted = 1 AND deletedAt = :deletedTime");
	$selectDeletedData->execute(array(":deletedTime" => $deletedTime));

	$deletedData = $selectDeletedData->fetchAll(PDO::FETCH_ASSOC);

	if(count($deletedData) > 0){
		$backupButton = "<button name='backup' class='form-control btn btn-primary'>Gelöschte Nachrichten wiederherstellen!</button>";
	}
	


}catch(EXCEPTION $z){
			echo "Ein Fehler ist aufgetreten: " . $z->getMessage();
			exit();
}


?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Terminverwaltung</title>

	<!-- Javascript-Datei einbinden -->
  	<script src="https://code.jquery.com/jquery-latest.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  	<script src="../js/main.js"></script>

	<!-- CSS-Datei einbinden -->
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="../../libraries/bootstrap/css/bootstrap.min.css">
	<script defer src="../../libraries/fontawesome/static/js/fontawesome-all.js"></script>

	<style>
		button:hover{
			cursor: pointer;
		}

		a.nav-link:hover{
			color: black !important;
		}


	</style>
</head>
<body>
	<header>
		<nav class="navbar bg-success p-3">
           <a class="navbar-brand text-white nav-link" href="../adminpanel.php">Zurück</a>
        </nav>
	</header>
	<main>
		<section class="container-fluid mt-5">
			<h1 class="text-center">Terminverwaltung</h1>
			<div class="row mt-5">
				<div class="col-sm-6 col-12">
					<form method="post" class="mb-3">
						<div class="form-group">
							<label>Termingrund:</label>
							<input type="text" name="termingrund" class="form-control">
						</div>
						
						<div class="form-group">
							<label>Terminbeschreibung:</label>
							<textarea name="terminbeschreibung" rows="15" class="form-control"></textarea>
						</div>
						
						<div class="form-group">
							<label>Datum:</label>
							<input type="text" name="datum" id="datum"  class="form-control" readonly>
						</div>
						
						<div class="form-group">
							<label>Uhrzeit:</label>
							<input type="text" name="uhrzeit" class="form-control"> <i class="fas fa-info-circle"></i><span> Das Format der Uhrzeit ist: 00:00</span>
						</div>
						
						
						

						<button name="submitted" id="submit" class="form-control btn btn-success">Termin erstellen!</button>
					</form>

					<?php
						if(isset($fehler)){
							echo $fehler;
						}
					?>
				</div>
				<div class="col-sm-6 col-12">
					<h2 class="text-center">Alle Termine: </h2>

					<form method="post" class="mb-3">
					<?php 
						foreach ($alleTermine as $key => $value) {

							$key += 1;

							$zeitangaben = explode(" ", $value["datum"]);
							
							$day = new DateTime($zeitangaben[0]);
							$day = $day->format("d.m.Y");
							
							$time = substr($zeitangaben[1], 0, 5);

							
							echo "<div class='mb-3'>";
								echo "<ul class='list-group'>";
										echo "<li class='list-group-item border-blue bg-primary text-dark font-weight-bold'>";
											echo "<label class='col-11 mr-1'>Termin ". $key . "</label>";
												echo "<div class='custom-control ml-3'>";
													echo "<input type='checkbox' name='id[]' value='" . htmlspecialchars($value['terminId']) . "'>";
												echo "</div>";
										echo "</li>";
										echo "<li class='list-group-item border-blue'>Termingrund: " . htmlspecialchars($value["grund"]) . "</li>";
										echo "<li class='list-group-item border-blue'>Beschreibung: " . htmlspecialchars($value["beschreibung"]) . "</li>";
										echo "<li class='list-group-item border-blue'>Am: " . htmlspecialchars($day) . "</li>";
										echo "<li class='list-group-item border-blue'>Uhrzeit: " . htmlspecialchars($time) . " Uhr</li>";
									echo "</ul>";
							echo "</div>";
								
						}

					?>
						<div class="row mb-3">
							<div class="col-6">
								<button name="deleteTermin" class="form-control btn btn-danger">Löschen!</button>
							</div>
							<div class="col-6">
								<button name="editTermin" class="form-control btn btn-warning">Editieren!</button>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<?php
									if(isset($backupButton)){
										echo $backupButton;
									}
								?>
							</div>
						</div>
					</form>
					<?php
						if(isset($fehler2)){
							echo $fehler2;
						}
					?>
				</div>
			</div>
			
			
		</section>
	</main>
	<footer class="text-center mt-5">
		<p> &copy; Stef 2018</p>
	</footer>
</body>
</html>