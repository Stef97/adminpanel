<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/

session_start();

if(!isset($_SESSION['userId'])){

	echo "<p class='text-danger text-center'>Sie müssen sich erst <a href='../../../index.php'>hier</a> einloggen!</p>";
	exit();

} else if(isset($_SESSION["rang"])){

	if($_SESSION['rang'] !== "admin"){
		echo "<p class='text-danger text-center'>Dieser Bereich ist für Sie nicht zugängig!</p>";
		exit();
	}
}

$userId = $_GET['id'];

if(file_exists("../../../components/config/dbConnection.php")){
	require_once("../../../components/config/dbConnection.php");
}

try{
	
	$selectUser = $dbv->prepare("SELECT username, rang FROM users WHERE userId = :id LIMIT 1");
	$selectUser->execute(array(":id" => $userId));

	$userData = $selectUser->fetch(PDO::FETCH_ASSOC);

	if(isset($_POST['userEdited'])){

		$username = isset($_POST['username']) ? $_POST['username'] : null;
		$rang = isset($_POST['rang']) ? $_POST['rang'] : null;
		$password = isset($_POST["newPassword"]) ? $_POST["newPassword"] : null;

		if(empty($username) || empty($rang)){
			$fehler = "<p class='text-danger text-center'>Bitte füllen Sie alle Felder aus!</p>";
		}

		if(!isset($fehler)){

			$pepper = "!#+?=45&/()";
			$password .= $pepper;
			$password = password_hash($password, PASSWORD_DEFAULT);

			if(empty($password)){

				$updateUser = $dbv->prepare("UPDATE users SET username = :username, avatar = :avatar, rang = :rang WHERE userId = :userId");
				$updateUser->execute(
					array(
						":username" => $username,
						":rang" => $rang,
						":userId" => $userId
					)
				);

			} else if(isset($password)){

				$updateUser = $dbv->prepare("UPDATE users SET username = :username, avatar = :avatar, rang = :rang, passwort = :password WHERE userId = :userId");
				$updateUser->execute(
					array(
						":username" => $username,
						":rang" => $rang,
						":password" => $password,
						":userId" => $userId
					)
				);

			}
			

			header("Location: ../manageUser.php");
		}
	}

} catch(EXCEPTION $e){
	echo "Ein Fehler ist aufgetreten: " . $e->getMessage();
	exit();
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Edit User</title>

	<link rel="stylesheet" href="../../../libraries/bootstrap/css/bootstrap.min.css">

	<style>
		button:hover{
			cursor: pointer;
		}

		a.nav-link:hover{
			color: black !important;
		}
	</style>
</head>
<body>
	<header>
		<nav class="navbar bg-warning p-3">
           <a class="navbar-brand text-white nav-link" href="../manageUser.php">Zurück</a>
        </nav>
	</header>
	<main>
		<section class="container mt-5">
			<h1 class="text-center">Edit User</h1>
				<form method="post" class="mb-3">
					<?php
						foreach ($userData as $key => $value) {
							
							if($key == "rang"){
								echo  "<div class='form-group'>";
									echo "<label>". ucfirst($key) . ":</label><br>";
										echo "<select name='". $key . "' class='custom-select'>";
											echo "<option value='" . htmlspecialchars($value) . "' selected>" . htmlspecialchars(ucfirst($value)) . "</option>";
											if($value == "admin"){
												echo "<option value='user'>User</option>";
											} else{
												echo "<option value='admin'>Admin</option>";
											}
											
											
										echo "</select>";
								echo "</div>";
							} else{
								echo "<div class='form-group'>";
									echo "<label>" . ucfirst($key) . ": </label> <input type='text' name='" . $key . "' value='" . htmlspecialchars($value) . "' class='form-control'>" ;
								echo "</div>";
							}
						}
					?>
					<div class="form-group">
						<label>Neues Passwort:</label> <input type="password" name="newPassword" class="form-control">
					</div>

				
				<button name="userEdited" class="form-control btn btn-warning">User abspeichern</button>

				
				</form>
				<?php
					if(isset($fehler)){
						echo $fehler;
					}
				?>
		</section>
	</main>
	<footer class="mt-5">
		<p class="text-center"> &copy; Stef 2018</p>
	</footer>
</body>
</html>