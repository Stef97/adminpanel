<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/
session_start();

if(!isset($_SESSION['userId'])){

	echo "<p class='text-danger text-center'>Sie müssen sich erst <a href='../../index.php'>hier</a> einloggen!</p>";
	exit();

} else if(isset($_SESSION["rang"])){

	if($_SESSION['rang'] !== "admin"){
		echo "<p class='text-danger text-center'>Dieser Bereich ist für Sie nicht zugängig!</p>";
		exit();
	}
}

if(file_exists("../../../components/config/dbConnection.php")){
	require_once("../../../components/config/dbConnection.php");
}

	
	if(isset($_GET["get"]) && $_GET["get"] == "exportUsers"){

		$value = $_GET["get"];

		$stmt = $dbv->prepare("SELECT userId, username, rang FROM users");
		$stmt->execute();

		$allUsers = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$csv = fopen("userList.csv", "w");

		$header = array("User-Id", "Username", "Rang");
		$spaceBetween = array("");

		fputcsv($csv, $header, ";");
		fputcsv($csv, $spaceBetween);

		foreach ($allUsers as $key => $value) {
			
			fputcsv($csv, $value, ";");
		}

		echo "<p class='text-success'>Die Users wurden erfolgreich in die CSV-Datei exportiert. <a href='exportToCsv/userList.csv' class='text-success'>CSV-Datei abspeichern!</a></p>";

		fclose($csv);


	}