<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/

session_start();


if(!isset($_SESSION['userId'])){

	echo "<p class='text-danger text-center'>Sie müssen sich erst <a href='../../../index.php'>hier</a> einloggen!</p>";
	exit();

} else if(isset($_SESSION["rang"])){

	if($_SESSION['rang'] !== "admin"){
		echo "<p class='text-danger text-center'>Dieser Bereich ist für Sie nicht zugängig!</p>";
		exit();
	}
}

$terminId = $_GET['id'];

if(file_exists("../../../components/config/dbConnection.php")){
	require_once("../../../components/config/dbConnection.php");
}

if(file_exists("../../../components/functions.php")){
	require_once("../../../components/functions.php");
}

try{

	$selectTermine = $dbv->prepare("SELECT grund, beschreibung, datum FROM termine WHERE terminId = :terminId LIMIT 1");
	$selectTermine->execute(array(":terminId" => $terminId));

	$termin = $selectTermine->fetch(PDO::FETCH_ASSOC);


	if(isset($_POST['changeTermin'])){

		$grund = isset($_POST['grund']) ? $_POST['grund'] : null;
		$beschreibung = isset($_POST['beschreibung']) ? $_POST['beschreibung'] : null;
		$datum = isset($_POST['datum']) ? $_POST['datum'] : null;
		$uhrzeit = isset($_POST['uhrzeit']) ? $_POST['uhrzeit'] : null;

		if(isset($datum) && isset($uhrzeit)){
			$date = $datum . " " . $uhrzeit . ":00";
		}

		$dateNow = date("Y-m-d H:i:s");

		if(empty($grund) || empty($beschreibung) || empty($datum) || empty($uhrzeit)){
			$fehler = "<p class='text-danger text-center'>Bitte füllen Sie alle Felder aus!</p>";
		} else if(strtotime($date) < strtotime($dateNow)){

			$fehler = "<p class='text-danger text-center'>Das Datum und die Uhrzeit müssen in der Zukunft liegen!</p>";

		} else if(!checkValidTime($uhrzeit)){

			$fehler = "<p class='text-danger text-center'>Bitte geben Sie eine gültige Uhrzeit an!</p>";
		}

		if(!isset($fehler)){

			$datum = new DateTime($date);
			$datum = $datum->format("Y-m-d H:i:s");
			


			$updateTermin = $dbv->prepare("UPDATE termine SET grund = :grund, beschreibung = :beschreibung, datum = :datum WHERE terminId = :terminId");
			$result = $updateTermin->execute(
					array(
						":grund" => $grund,
						":beschreibung" => $beschreibung,
						":datum" => $datum,
						":terminId" => $terminId
					)
			);

			if($result){
			
				header("Location: ../diary.php");
			}

		}
	}


}catch(EXCEPTION $e){
	echo "Ein Fehler ist aufgetreten: " . $e->getMessage();
	exit();
}


?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Termin editieren</title>

	<!-- Javascript-Datei einbinden -->
  	<script src="https://code.jquery.com/jquery-latest.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  	<script src="../../js/main.js"></script>

	<!-- CSS-Datei einbinden -->
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="../../../libraries/bootstrap/css/bootstrap.min.css">
	<script defer src="../../../libraries/fontawesome/static/js/fontawesome-all.js"></script>

	

	<style>

		a.nav-link:hover{
			color: black !important;
		}

		button:hover{
			cursor: pointer;
		}
	</style>
</head>
<body>
	<header>
		<nav class="navbar bg-warning p-3">
           <a class="navbar-brand text-white nav-link" href="../diary.php">Zurück</a>
        </nav>
	</header>
	<main>
		<section class="container mt-5">
			<h1 class="text-center">Edit Termin</h1>
				<form method="post" class="mb-3">
					<?php
						if(isset($termin)){

							$date = explode(" ", $termin["datum"]);
							
							$datum = new DateTime($date[0]);
							$datum = $datum->format("d.m.Y");

							$time = substr($date[1], 0, 5);

							foreach ($termin as $key => $value) {
								
								if($key == "datum"){
									echo "<div class='form-group'>";
									 	echo "<label>". ucfirst(htmlspecialchars($key))  . ": </label>
										<input type='text' name='". htmlspecialchars($key) . "' value='". htmlspecialchars($datum) . "' id='date' readonly class='form-control'>";
									echo "</div>";
								} 

								if($key == "beschreibung"){
									echo "<div class='form-group'>";
										echo "<label>". ucfirst(htmlspecialchars($key))  . ": </label>
										<textarea name='". htmlspecialchars($key) . "' value='". htmlspecialchars($value) . "' cols='15'class='form-control'>" . htmlspecialchars($value) ." </textarea>";
									echo "</div>";

								} 

								if($key != "beschreibung" && $key != "datum"){
									echo "<div class='form-group'>";
										echo "<label>". ucfirst(htmlspecialchars($key))  . ": </label>
										<input type='text' name='". htmlspecialchars($key) . "' value='". htmlspecialchars($value) . "'class='form-control'>";
									echo "</div>";
								}
				
							}
												
					}
						
					?>
					
					<div class="form-group">
						<label>Uhrzeit: </label>
						<input type='text' name='uhrzeit' value='<?php echo htmlspecialchars($time); ?>' class='form-control'> <i class="fas fa-info-circle" id="info"></i><span> Das Format der Uhrzeit ist: 00:00</span>
					</div>
					

					<button name="changeTermin" class='form-control btn btn-warning'>Termin ändern!</button>
				</form>
				<?php
					if(isset($fehler)){
						echo $fehler;
					}
				?>
		</section>
	</main>
	<footer class="mt-5">
		<p class="text-center"> &copy; Stef 2018</p>
	</footer>
</body>
</html>