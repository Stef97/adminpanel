<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/

session_start();

if(!isset($_SESSION['userId'])){

	echo "<p class='text-danger text-center'>Sie müssen sich erst <a href='../../index.php'>hier</a> einloggen!</p>";
	exit();

} else if(isset($_SESSION["rang"])){

	if($_SESSION['rang'] !== "admin"){
		echo "<p class='text-danger text-center'>Dieser Bereich ist für Sie nicht zugängig!</p>";
		exit();
	}
}

if(file_exists("../../components/config/dbConnection.php")){
	require_once("../../components/config/dbConnection.php");
}

try{

	if(isset($_POST['deleteUser'])){

		

		$userId = isset($_POST['deleteUser']) ? $_POST['deleteUser'] : null;

		if(empty($userId)){
			$fehler2 = "<p class='text-danger text-center'>Bitte wählen Sie einen User aus um ihn löschen zu können!</p>";
		}else{
			$deleteUser = $dbv->prepare("DELETE FROM users WHERE userId = :userId");
			$result = $deleteUser->execute(array(":userId" => $userId));

			if($result){

				$erfolgreich2 = "<p class='text-success text-center'>Der User wurde gelöscht!</p>";
			}

			
		}
	} 
	 else if(isset($_POST['editUser'])){

		$userId = isset($_POST['editUser']) ? $_POST['editUser'] : null;

		if(empty($userId)){
			$fehler2 = "<p class='text-danger text-center'>Bitte wählen Sie einen User aus um ihn editieren zu können!</p>";
		}else{
			header("LOCATION: user/edit_user.php?id=" . $userId);
		}

		
	}

} catch(EXCEPTION $e){
	echo "Ein Fehler ist aufgetreten: " . $e->getMessage();
	exit();
}



if(isset($_POST['submit'])){

	$username = filter_var(ucfirst($_POST['username']), FILTER_SANITIZE_STRING);
	$rang = $_POST['rang'];

	$pepper = "!#+?=45&/()";
	$passwort = $_POST['passwort'];
	$passwort_wiederholung = $_POST['passwort_wiederholung'];

	if(empty($username) || empty($rang) || empty($passwort) || empty($passwort_wiederholung)){
		$fehler = "<p class='text-danger text-center'>Bitte füllen Sie alle Felder aus!</p>";
	} else if($passwort !== $passwort_wiederholung){
		$fehler = "<p class='text-danger text-center'>Die Passwörter stimmen nicht überein!</p>";
	}

	$passwort .= $pepper;
	$password_gehasht = password_hash($passwort, PASSWORD_DEFAULT);

	if(!isset($fehler)){

		try{

			$stmt = $dbv->prepare("INSERT INTO users(username, avatar, rang, passwort) VALUES (:username, :avatar, :rang, :passwort)");
			$result = $stmt->execute(
						array(
							":username" => $username,
							":avatar" => "images/avatar.png",
							":rang" =>	$rang,
							":passwort" => $password_gehasht

						)
					);

			if($result){
				$erfolgreich = "<p class='text-success text-center'>User wurde hinzugefügt</p>";
			} else{
				$fehler = "<p class='text-danger text-center'>Es ist ein Fehler aufgetreten! Bitte wenden Sie sich an den Webseitenersteller</p>";
			}



		}catch(EXCEPTION $w){
			echo "Ein Fehler ist aufgetreten: " . $w->getMessage();
			exit();
		}
	}
}

// Ausgewählter User aus DB löschen und editieren + alle User aus der DB fetchen
try{

	$countUserIds = $dbv->prepare("SELECT COUNT(userId) FROM users");
	$countUserIds->execute();

	
	$page = 1;
	
	if(isset($_GET["page"])){
		$page = (int)$_GET["page"];
	} 

	$countIds = $countUserIds->fetch();
	$userCount = $countIds[0];
	

	$limit = 5;
	$offset = 0;
	
	if($page >= 1){
		$offset = ($page-1) * $limit; 
	}

	if($offset >= $userCount){
		
		$offset = 0;
	}

	

	$selectUsers = $dbv->prepare("SELECT userId, username, avatar, rang FROM users LIMIT :offset, :start");
	$selectUsers->bindParam('start', $limit, PDO::PARAM_INT);
	$selectUsers->bindParam('offset', $offset, PDO::PARAM_INT);
	
	$selectUsers->execute();
	
	$allUsers = $selectUsers->fetchAll(PDO::FETCH_ASSOC);
	
	$totalPages = ceil($userCount / $limit);

	$previousPage = $page - 1;
	$nextPage = $page + 1;

	if($page == 1){
		$previous =  "<li class='page-item'><a href='#' class='page-link'>Previous</a></li>";
	} else{
		$previous = "<li class='page-item'><a href='manageUser.php?page=" . $previousPage . "' class='page-link'>Previous</a></li>";
	}

	for($i = 1; $i <= $totalPages; $i++){
		$links[] = "<li class='page-item'><a href='manageUser.php?page=" . $i . "' class='page-link'>$i</a></li>";
	} 

	if($page >= $totalPages){
		$next = "<li class='page-item'><a href='#' class='page-link'>Next</a></li>";
	}else{
		$next = "<li class='page-item'><a href='manageUser.php?page=" . $nextPage . "' class='page-link'>Next</a></li>";
	}
	
	$pagination = "<div class='row'>
						<ul class='pagination'>
							". $previous  . "
					";

	foreach ($links as $key => $value) {
		$pagination .= $value;
	}
	$pagination .= 	 $next	." 
						</ul>
					</div>";

}catch(EXCEPTION $z){
			echo "Ein Fehler ist aufgetreten: " . $z->getMessage();
			exit();
}




?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>User verwalten</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script defer src="../../libraries/fontawesome/static/js/fontawesome-all.js"></script>

	<style>

		img{
			height: 2em;
			width: 2em;
		}

		button:hover{
			cursor: pointer;
		}

		a.nav-link:hover{
			color: black !important;
		}

		a.functionLinks, a.profilLink{
			text-decoration: none;
		}

		a.profilLink{
			color: black;
		}

		a.profilLink:hover{
			color: blue;
		}

	</style>

	<script>
		   window.onload = function(){

            	var addNewUser = document.getElementById("addNewUser");
            	var exportUserList = document.getElementById("exportUserList");
            	
            	addNewUser.addEventListener("click", showFormular);
            	exportUserList.addEventListener("click", exportToCsv);

	            function showFormular(){
	                
	                var ausgabeBereich = document.getElementById("userForm");

	                var ausgabe = '<form method="post">' + 
												'<div class="row p-3">' +
													'<div class="col-sm-1"> <p></p> </div>' + 
												'<div class="col-sm-2">' +
													"<div class='form-group'>" + 
														"<input type='text' name='username' class='form-control' placeholder='Username'>" + 
													"</div>" + 
												"</div>" + 
												'<div class="col-sm-2">'  +
													'<div class="form-group">' +
														'<select name="rang" class="custom-select">' +
															'<option value="" selected>-</option>' +
															'<option value="user">User</option>' +
															'<option value="admin">Admin</option>' +
														'</select> ' +
													'</div>' +
												'</div>' +
												'<div class="col-sm-3">' +
													'<div class="form-group">' +
														'<input type="password" name="passwort" class="form-control" placeholder="Password">' +
													'</div>' +
												'</div>' +
												'<div class="col-sm-4">' +
													'<div class="form-group">' +
														'<input type="password" name="passwort_wiederholung" class="form-control" placeholder="Password wiederholen">' +
													'</div>' +
												'</div>' +
											
										'</div>' +
										'<div class="row">' +
											'<button class="col-12 btn btn-success" name="submit">User hinzufügen</button>' +
										'</div>' +
									'</form>';

					ausgabeBereich.innerHTML = ausgabe;
	               
	                
	            }
	           


	            function exportToCsv(){

	            	var linkValue = document.getElementById("exportUserList").getAttribute('value');
	            	

	            	var request = new XMLHttpRequest();

	            	request.addEventListener("load", function(){
	            		document.getElementById("ausgabe").innerHTML = this.responseText;

	            	});

	        
	            	request.open("GET", "exportToCsv/exportUsers.php?get=" + linkValue, true);
	            	request.send();
	            }
        }
	</script>
	
</head>
<body>
	<header>
		<nav class="navbar bg-success p-3">
           <a class="navbar-brand text-white nav-link" href="../adminpanel.php">Zurück</a>
        </nav>
	</header>
	<main>
		<section class="container mt-5">
				<div class="row bg-primary p-2">
					<div class="col-sm-4">
						<h1>Userverwaltung</h1>
					</div>
				</div>
				<div class="row mb-3">
					<div class="col-sm-12 col-6 p-2 ">
						<a href="#" class="btn-btn-success mr-3 bg-warning p-2 functionLinks" id="addNewUser">
							<i class="fas fa-user-plus"></i>
							<span>User hinzufügen</span>
						</a>
					</div>
					<div class="col-sm-12 col-6 p-2">
						<a href="#" class="btn-btn-success bg-warning p-2 functionLinks" id="exportUserList" value="exportUsers">
							<i class="far fa-file-excel"></i>
							<span>Userliste exportieren</span>
						</a>
					</div>
				</div>
				<div class="row p-3">
					<div class="col-sm-1 d-none d-sm-block">
						<p>#</p>
					</div>
					<div class="col-sm-2 col-3">
						<p>Username</p>
					</div>
					<div class="col-sm-2 col-2">
						<p>Rang</p>
					</div>
					<div class="col-sm-5 col-3">
						<p>Aktivität</p>
					</div>
					<div class="col-sm-2 col-4">
						<p>Aktionen</p>
					</div>
				</div>
				<?php
					
					
					$paginationLinks = count($links);
					
					foreach ($allUsers as $key => $value) {

						$key += 1;
						
						if(isset($page)){

							foreach ($links as $linkKey => $linkValue) {

								$linkKey += 1;

								/*if($page == 1){
									$key += 0;
								}
								else if($page > $paginationLinks){
									$key += 0;
									
								} else if($page > $linkKey){
									$key += 5;
								}*/

								$pageCheck = ( $page == 1 || $page > $paginationLinks );
								$key += ( !$pageCheck && $page > $linkKey ) ? 5 : 0;
							}		
						} 
							
					

						echo 	"<div class='row p-3'>	
									<div class='col-sm-1 d-none d-sm-block'>
										<p>" . $key . "</p>
									</div>
								  	<div class='col-sm-2 col-3'>
										<a href='../../user/php/profil.php?id=" . htmlspecialchars($value['userId']) . "' class='profilLink'>
											<img src='../../user/". htmlspecialchars($value["avatar"]) . "' alt='userPic' class='rounded-circle'>   ". htmlspecialchars(ucfirst($value["username"])) . "
										</a>	
									</div>
									<div class='col-sm-2 col-2'>
										<p>". htmlspecialchars(ucfirst($value["rang"])) . "</p>
									</div>
									<div class='col-sm-5 col-3'>
										<i class='fas fa-circle fa-xs text-success'></i><span> Aktiv</span>
									</div>
									<div class='col-sm-2 col-4'>
										<form method='post'>
											
												<button class='btn btn-warning' title='User editieren' value='" . htmlspecialchars($value['userId']) . "' name='editUser'>
												<i class='far fa-edit'></i></button> 
											
											
												<button class='btn btn-danger' title='User löschen' value='" . htmlspecialchars($value['userId']) . "' name='deleteUser'>
												<i class='far fa-times-circle'></i></button>
											
										</form>
									</div>
								</div>";


					}
				?>
				
				<div id="userForm">
					
				</div>
				<?php
					if(isset($fehler)){
						echo $fehler;
					} else if(isset($fehler2)){
						echo $fehler2;
					}
				?>
				<div id="ausgabe">
					
				</div>

				<?php
					echo "<div class='row'>
							<div class='col-sm-12'>
								<p>" . $key . " von " . $userCount . " User</p>
							</div>
						</div>
					";

					if(isset($pagination)){
							
						echo $pagination;
					}
				?>
			
			
			

		</section>
	</main>
	<footer class="mt-5">
		<p class="text-center"> &copy; Stef 2018</p>
	</footer>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>


</html>