<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/

session_start();

if(!isset($_SESSION['userId'])){

	echo "<p class='text-danger text-center'>Sie müssen sich erst <a href='../../index.php'>hier</a> einloggen!</p>";
	exit();

} else if(isset($_SESSION["rang"])){

	if($_SESSION['rang'] !== "admin"){
		echo "<p class='text-danger text-center'>Dieser Bereich ist für Sie nicht zugängig!</p>";
		exit();
	}
}

if(file_exists("../../components/config/dbConnection.php")){
	require_once("../../components/config/dbConnection.php");
}

try{
	
	$deletedTime = date("Y-m-d");

	if(isset($_POST['deleteMessage'])){

		$messageId = isset($_POST['messageId']) ? $_POST['messageId'] : null;
		

		if(empty($messageId)){
			$fehler2 = "<p class='text-danger text-center'>Bitte wählen sie die Nachricht aus welchen sie löschen möchten!</p>";
		} else{

			foreach ($messageId as $key => $messageId) {

				$deleteStmt = $dbv->prepare("UPDATE messages SET isDeleted  = :isDeleted, deletedAt = :deletedAt WHERE messageId = :messageId");
				$result1 = $deleteStmt->execute(
						array(
							":isDeleted" => 1,
							":deletedAt" => $deletedTime,
							":messageId" => $messageId
						)
					);
			}
			

			if($result1){
				$success2 = "<p class='text-success text-center'>Die Nachricht wurde erfolgreich gelöscht!</p>";
			}			
		}	
	} else if(isset($_POST['backup'])){

		$backupStmt = $dbv->prepare("UPDATE messages SET isDeleted = 0 WHERE deletedAt = :deletedAt");
		$result2 = $backupStmt->execute(array(":deletedAt" => $deletedTime));

		if($result2){
				$success2 = "<p class='text-success text-center'>Gelöschte Nachrichten wurden wiederhergestellt!</p>";
		}
	}
	

} catch(EXCEPTION $e){
	echo "Ein Fehler ist aufgetreten: " . $e->getMessage();
	exit();
}

if(isset($_POST['createMessage'])){

	$messageHead = isset($_POST['messageReason']) ? $_POST['messageReason'] : null;
	$message = isset($_POST['message']) ? $_POST['message'] : null;

	if(empty($message) || empty($messageHead)){
		$fehler = "<p class='text-danger text-center'>Bitte füllen Sie alle Felder aus!</p>";
	}

	if(!isset($fehler)){

		try{

			$insertQuery = "INSERT INTO messages (senderId, messageHead, message, isDeleted, deletedAt) VALUES (:senderId, :messageHead, :message, :isDeleted, :deletedAt)";
			$insertStmt = $dbv->prepare($insertQuery);

			$result = $insertStmt->execute(
					array(
						"senderId" => $_SESSION['userId'],
						":messageHead" => $messageHead, 
						":message" => $message,
						":isDeleted" => 0,
						"deletedAt" => null
					)
			);

			if($result){
				$success = "<p class='text-success text-center'>Die Nachricht wurde erstellt!</p>";
			} else{
				$fehler = "<p class='text-danger text-center'>Es ist ein Fehler bei der Erstellung der Nachricht entstanden!</p>";
			}

		} catch(EXCEPTION $a){
			echo "Ein Fehler ist aufgetreten: " . $a->getMessage();
			exit();
		}
	}
}

try{

	$selectAllMessages =	$dbv->prepare("SELECT u.username, 
												  m.messageId, 
												  m.messageHead,
												  m.message,
												  m.datum
											FROM 
												users u 
											LEFT JOIN 
												messages m ON m.senderId = u.userId
											WHERE
												m.isDeleted = 0");

	$selectAllMessages->execute();


	$allMessages = $selectAllMessages->fetchAll(PDO::FETCH_ASSOC);

	$selectDeletedData = $dbv->prepare("SELECT messageId FROM messages WHERE isDeleted = 1 AND deletedAt = :deletedTime");
	$selectDeletedData->execute(array(":deletedTime" => $deletedTime));

	$deletedData = $selectDeletedData->fetchAll(PDO::FETCH_ASSOC);

	if(count($deletedData) > 0){
		$backupButton = "<button name='backup' class='form-control btn btn-primary'>Gelöschte Nachrichten wiederherstellen!</button>";
	}


} catch(EXCEPTION $w){
	echo "Ein Fehler ist aufgetreten: " . $w->getMessage();
	exit();
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Nachrichtenzentrale</title>

	<link rel="stylesheet" href="../../libraries/bootstrap/css/bootstrap.min.css">

	<style>
		button:hover{
			cursor: pointer;
		}

		a.nav-link:hover{
			color: black !important;
		}
	</style>
</head>
<body>
	<header>
		<nav class="navbar bg-success p-3">
           <a class="navbar-brand text-white nav-link" href="../adminpanel.php">Zurück</a>
        </nav>
	</header>
	<main>
		<section class="container-fluid mt-5">
			<h1 class="text-center">Nachrichtenzentrale</h1>
			<div class="row mt-5">
				<div class="col-sm-6 col-12">
					<form method="post" class="mb-3"> 
						<div class="form-group">
							<label>Nachrichtengrund:</label>
							<input type="text" name="messageReason" class="form-control">
						</div>
						<div class="form-group">
							<label>Nachricht:</label>
							<textarea name="message" class="form-control" rows="15"></textarea>
						</div>
					
						<button name="createMessage" class="form-control btn btn-success">Nachricht erstellen!</button>
					</form>
						<?php
							if(isset($fehler)){
								echo $fehler;
							} else if(isset($success)){
								echo $success;
							}
						?>
				</div>
				<div class="col-sm-6 col-12">
					<h2 class="text-center">Alle Nachrichten:</h2>
						<form method="post" class="mb-3">
								<?php
									if(isset($allMessages) ){
										foreach ($allMessages as $key => $value) {

											$key += 1;

											$datetime = explode(" ", $value["datum"]);

											$tag = new DateTime($datetime[0]);
											$tag = $tag->format("d.m.Y");

											$uhrzeit = $datetime[1];
											
											echo "<div class='mb-3'>";
												echo "<ul class='list-group'>";
													echo "<li class='list-group-item border-blue bg-primary text-dark font-weight-bold'>";
														echo "<label class='col-11 mr-1'>Nachricht ". $key . "</label>";
															echo "<div class='custom-control ml-3'>";
																echo "<input type='checkbox' value='" . htmlspecialchars($value['messageId']) . "' name='messageId[]'>";
															echo "</div>";	
													echo "</li>";
													echo "<li class='list-group-item border-blue'>Nachrichtengrund: " . htmlspecialchars($value["messageHead"]) . "</li>";
													echo "<li class='list-group-item border-blue'>Nachricht: " . htmlspecialchars($value['message']) . "</li>";
													echo "<li class='list-group-item border-blue'>Erstellt am " . htmlspecialchars($tag) . " um " . htmlspecialchars($uhrzeit) . " Uhr durch ". htmlspecialchars($value["username"]) . "</li>";
												echo "</ul>";
											echo "</div>";
											
												
										}
									}
								?>
							
							<div class="row mt-5">
								<div class="col-sm-12">
									<button name="deleteMessage" class="form-control btn btn-danger">Nachricht löschen!</button>
								</div>
								<div class="col-sm-12 mt-3">
									<?php
										if(isset($backupButton)){
											echo $backupButton;
										}
									?>
								</div>
							</div>
						</form>
						<?php
							if(isset($fehler2)){
								echo $fehler2;
							} else if(isset($success2)){
								echo $success2;
							}
						?>
				</div>
			</div>
		</section>
	</main>
	<footer class="mt-5">
		<p class="text-center"> &copy; Stef 2018</p>
	</footer>
</body>
</html>