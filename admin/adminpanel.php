<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/
	session_start();

	if(!isset($_SESSION['userId'])){

		echo "<p class='text-danger text-center'>Sie müssen sich erst <a href='../index.php'>hier</a> einloggen!</p>";
		exit();

	} else if(isset($_SESSION["rang"])){

		if($_SESSION['rang'] !== "admin"){
			echo "<p class='text-danger text-center'>Dieser Bereich ist für Sie nicht zugängig!</p>";
			exit();
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Adminpanel</title>

	 <link rel="stylesheet" href="../libraries/bootstrap/css/bootstrap.min.css">

	 <style>
	 	.cardImages{
	 		width: 100%;
	 		height: 50%;
	 	}

	 	a.nav-link:hover{
			color: black !important;
		}
	 </style>
</head>
<body>
	<header>
		<nav class="navbar bg-success p-3">
           <a class="navbar-brand text-white nav-link" href="../user/home.php">Zurück</a>
        </nav>
	</header>
	<main>
		<section class="container mt-5">
			<h1 class="text-center">Adminpanel</h1>
			<div class="card-deck text-center mt-5">
				<div class="card">
					<img src="images/user.png" alt="userPic" class="card-img-top cardImages">
					<div class="card-body p-3">
						<p class="card-title"><a  href="php/manageUser.php">Userverwaltung</a></p>
						<p class="card-deck">Hier können Sie alle User sehen sowie erstellen, editieren und löschen</p>
					</div>
				</div>
				<div class="card">
					<img src="images/message.jpg" alt="messagePic" class="card-img-top cardImages">
					<div class="card-body p-3">
						<p class="card-title"><a href="php/messages.php">Nachrichtenverwaltung</a></p>
						<p class="card-deck">Hier können Sie alle Nachrichten sehen sowie erstellen, editieren und löschen</p>
					</div>
				</div>
				<div class="card">
					<img src="images/termine.jpg" alt="kalenderPic" class="card-img-top cardImages">
					<div class="card-body p-3">
						<p class="card-title"><a href="php/diary.php">Terminverwaltung</a></p>
						<p class="card-deck">Hier können Sie alle Termine sehen sowie erstellen, editieren und löschen</p>
					</div>
				</div>
			</div>
		</section>
	</main>
	<footer class="mt-5">
		<p class="text-center"> &copy; Stef 2018</p>
	</footer>
</body>
</html>