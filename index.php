<?php
/********************************************************************
 *
 * Autor:           Stef
 *
 * Kontakt:			http://www.html-seminar.de/forum/ws/user/21515-stef/
 * 
 * Copyright:		Copyright by Stef
 *
 * Info: Benutzung dieses Scripts ist nur mit den oben stehenden Daten erlaubt!
 ********************************************************************/
 
session_start();

if(file_exists("components/config/dbConnection.php")){
require_once("components/config/dbConnection.php");
}

if(isset($_POST["submitted"])){

	$username = isset($_POST['username']) ? ucfirst($_POST['username']) : null;
	$password = isset($_POST['passwort']) ? $_POST['passwort'] : null;

	$pepper = "!#+?=45&/()";

	if(empty($username) || empty($password)){
		$fehler = "<p class='text-danger text-center'>Bitte füllen Sie alle Felder aus!</p>";
	}

	if(!isset($fehler)){

		try{

			$selectUserData = $dbv->prepare("SELECT userId, username, passwort, rang FROM users WHERE username = :username LIMIT 1");
			$selectUserData->execute(array(":username" => $username));

			$userData = $selectUserData->fetch(PDO::FETCH_ASSOC);

			$password .= $pepper;

			if(password_verify($password, $userData['passwort'])){

				$_SESSION["userId"] = $userData['userId'];
				$_SESSION["rang"] = $userData['rang'];
				
				header("LOCATION: user/home.php");
				
			} else{
				$fehler = "<p class='text-danger text-center'>Sie haben einen falschen Username oder ein falsches Passwort eingegeben!</p>";
			}

		}catch(EXCEPTION $a){
			echo "Ein Fehler ist aufgetreten: " . $a->getMessage();
			exit();
		}
	}
}


?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Login</title>

	<link rel="stylesheet" href="libraries/bootstrap/css/bootstrap.min.css">

	<style>
		input[type="submit"]:hover{
			cursor: pointer;
		}
	</style>
</head>
<body>
	<div class="container mt-5">
		<header>
			<h1 class="text-center">Login</h1>
		</header>
		<main>
			<form method="post">
				<div class="form-group">
					<label>Benutzername:</label>
					<input type="text" name="username" class="form-control">
				</div>
				
				<div class="form-group">
					<label>Passwort:</label>
					<input type="password" name="passwort" class="form-control">
				</div>
				

				<input type="submit" name="submitted" value="Login!" class="btn btn-primary form-control mb-5">
			</form>

			<?php
				if(isset($fehler)){
					echo $fehler;
				}
			?>
		</main>
		<footer class="mt-5">
			<p class="text-center"> &copy; Stef 2018</p>
		</footer>
	</div>
</body>
</html>